package org.academiadecodigo.bootcamp;

public class Costumer {

    private String name;

    private Hotel myHotel;

    private int roomNr;

    public Costumer(String name, Hotel myHotel){
        this.name = name;
        this.myHotel = myHotel;
    }


    public String getName() {
        return name;
    }

    public void setMyHotel(Hotel myHotel) {
        this.myHotel = myHotel;
    }

    public void bookARoom(){
       roomNr = myHotel.checkIn();
    }

    public void leaveTheRoom(){
        myHotel.checkOut(roomNr);
    }

}
