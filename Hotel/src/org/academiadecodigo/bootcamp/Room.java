package org.academiadecodigo.bootcamp;

public class Room {

    private boolean isAvailable;


    Room(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean checkIfAvailable() {
        return isAvailable;
    }

}
