package org.academiadecodigo.bootcamp;

public class Main {

    public static void main(String[] args) {

        Hotel hotelCalifornia = new Hotel("Hotel California", 8);
        Costumer client1 = new Costumer("Rui", hotelCalifornia);
        Costumer client2 = new Costumer("Sara", hotelCalifornia);
        Costumer client3 = new Costumer("André", hotelCalifornia);
        Costumer client4 = new Costumer("Andreia", hotelCalifornia);
        Costumer client5 = new Costumer("Zézé", hotelCalifornia);
        Costumer client6 = new Costumer("NOZK", hotelCalifornia);
        Costumer client7 = new Costumer("Pedro", hotelCalifornia);
        Costumer client8 = new Costumer("Catarina", hotelCalifornia);
        Costumer client9 = new Costumer("Artur", hotelCalifornia);
        Costumer client10 = new Costumer("Tiago", hotelCalifornia);
        Costumer client11 = new Costumer("Ricardo", hotelCalifornia);
        Costumer client12 = new Costumer("Margarida", hotelCalifornia);

        System.out.println(client2.getName());
        client2.bookARoom();
        System.out.println(client1.getName());
        client1.bookARoom();
        client3.bookARoom();
        client4.bookARoom();
        client5.bookARoom();
        client6.bookARoom();
        client7.bookARoom();
        client8.bookARoom();
        client9.bookARoom();
        client2.leaveTheRoom();
        client10.bookARoom();
        client5.leaveTheRoom();
        client11.bookARoom();


    }
}
