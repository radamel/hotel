package org.academiadecodigo.bootcamp;

public class Hotel {

    private String name;
    private Room[] rooms;
    private static int NO_ROOM;


    public Hotel(String name, int numberOfRooms){
        this.name = name;
        this.rooms = new Room[numberOfRooms];

        for(int i = 0; i < rooms.length; i++){
            rooms[i] = new Room(true);
        }
    }

    public String getName() {
        return name;
    }
    public int checkIn(){

        for(int i = 0; i < rooms.length; i++) {

            if (rooms[i].checkIfAvailable()) {
                rooms[i].setAvailable(false);
                System.out.println("You booked room number " + (101 + i));
                return i;
            }

        }
        System.out.println("Sorry, our Hotel is full!");
        return NO_ROOM;
    }


    public void checkOut(int roomNr){

        rooms[roomNr].setAvailable(true);
        System.out.println("You left the room number " + (101 + roomNr));
        }

    }


